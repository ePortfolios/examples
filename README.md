# Project & Portfolio 1 #
## John D. Mack [ hacked for class example ] ##

### What is this repository for? ###

* This repository was created for the purpose of tracking and maintaining coursework for John Daniel Mack in the Project and Portfolio 1: Development course at Full Sail University.
* This portfolio will showcase all coursework and completed projects for this course and demonstrate my mastery of the material covered at the completion of this class.
* This file will be updated regularly as I progress through the current course and will reflect information relevant to my current achievements and completed projects.

### What's Included ##

- All completed projects from Scalable Data Infrastructures, previously completed at Full Sail University.
- Evidence of refractoring on original SDI projects, as required for coursework in Project and Portfolio 1.

+ **Debugging Projects** - _Source code, originally written by another person with multiple syntax and logical errors.  All errors have been corrected._
    + Mack-John-FindErrorsClasses
    + Mack-John-Find-Errors-Func
    + Mack-John-FindErrorsCond  

######  
* **Coding Projects**  - _Original source code, written based on specified requirements as outlined by course instructor._
    * Mack-John-Array-Lists
    * Mack-John-Arrays
    * Mack-John_Conditionals
    * Mack-John-CountFish
    * Mack-John-CustomClass
    * Mack-John-FinalProject
    * Mack-John-LogicLoops
    * Mack-John-MadLibs
    * Mack-John-Methods
    * Mack-John-RestaurantCalc
    * Mack-John-StringObjects
